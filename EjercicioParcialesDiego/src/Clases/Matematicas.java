/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.Scanner;

/**
 *
 * @author duvan
 */
public class Matematicas extends NotasAños {
    //deficion de variables para utilizar en el metodo calcular.
    private int contadorA;
    private double tarea;
    private double quiz;
    private double sumatarea;
    private double sumaquiz;
    private double totaltarea;
    private double totalquiz;
    private double p1;
    private double p2;
    private double p3;
    private double p4;
    private double promediototal;
    private double sumamateria;
    private double sumamateria2;
    private double promediomateria;
    //Libreria para aceptar valores por teclado
    Scanner Leer = new Scanner(System.in); 
    public void Calcular()
    {
        for(;;)
        {
          //Peticion de nota de los dos primeros parciales del alumno
          System.out.println("Digite la nota del primer parcial del alumno");
          setP1(Leer.nextDouble());
          System.out.println("Digite la nota del segundo parcial del alumno");
          setP2(Leer.nextDouble());
          //Calculando notas del parcial 3 con 5 tareas y 7 quiz
             for(int i=1; i<6;i++)
             {
                 //Peticion de la nota de la tarea
                 System.out.println("Ingrese la nota de la tarea numero " + i);
                 setTarea(Leer.nextDouble());
                 //calculando el total de las notas
                 setSumatarea(getSumatarea() + getTarea());
       
             }
             for(int i = 1 ; i<8;i++)
             {
                 //peticion de la nota del quiz
                 System.out.println("Digite la nota del quiz numero "+i);
                 setQuiz(Leer.nextDouble());
                 //calculando total de la nota
                 setSumaquiz(getSumaquiz() + getQuiz());
             }
             //calculando promedio de tareas
             setTotaltarea(getSumatarea() / 5);
             //calculando promedio de quiz
             setTotalquiz(getSumaquiz() / 7);
             //calculando tercer parcial
             setP3(getTotaltarea() + getTotalquiz());
             setP3(getP3() / 2);
             //digitando el valor del parcial 4
             System.out.println("Digite la nota del parcial 4");
             setP4(Leer.nextDouble());

             //Calculando el promedio total de este alumno en la materia
             setPromediototal(getP1()+getP2()+getP3()+getP4());
             setPromediototal(getPromediototal() / 4);
             break;
        }  
    }
    /**
     * @return the contadorA
     */
    public int getContadorA() {
        return contadorA;
    }

    /**
     * @param contadorA the contadorA to set
     */
    public void setContadorA(int contadorA) {
        this.contadorA = contadorA;
    }

    /**
     * @return the tarea
     */
    public double getTarea() {
        return tarea;
    }

    /**
     * @param tarea the tarea to set
     */
    public void setTarea(double tarea) {
        this.tarea = tarea;
    }

    /**
     * @return the quiz
     */
    public double getQuiz() {
        return quiz;
    }

    /**
     * @param quiz the quiz to set
     */
    public void setQuiz(double quiz) {
        this.quiz = quiz;
    }

    /**
     * @return the sumatarea
     */
    public double getSumatarea() {
        return sumatarea;
    }

    /**
     * @param sumatarea the sumatarea to set
     */
    public void setSumatarea(double sumatarea) {
        this.sumatarea = sumatarea;
    }

    /**
     * @return the sumaquiz
     */
    public double getSumaquiz() {
        return sumaquiz;
    }

    /**
     * @param sumaquiz the sumaquiz to set
     */
    public void setSumaquiz(double sumaquiz) {
        this.sumaquiz = sumaquiz;
    }

    /**
     * @return the totaltarea
     */
    public double getTotaltarea() {
        return totaltarea;
    }

    /**
     * @param totaltarea the totaltarea to set
     */
    public void setTotaltarea(double totaltarea) {
        this.totaltarea = totaltarea;
    }

    /**
     * @return the totalquiz
     */
    public double getTotalquiz() {
        return totalquiz;
    }

    /**
     * @param totalquiz the totalquiz to set
     */
    public void setTotalquiz(double totalquiz) {
        this.totalquiz = totalquiz;
    }

    /**
     * @return the p3
     */
    public double getP3() {
        return p3;
    }

    /**
     * @param p3 the p3 to set
     */
    public void setP3(double p3) {
        this.p3 = p3;
    }

    /**
     * @return the promediototal
     */
    public double getPromediototal() {
        return promediototal;
    }

    /**
     * @param promediototal the promediototal to set
     */
    public void setPromediototal(double promediototal) {
        this.promediototal = promediototal;
    }

    /**
     * @return the p4
     */
    public double getP4() {
        return p4;
    }

    /**
     * @param p4 the p4 to set
     */
    public void setP4(double p4) {
        this.p4 = p4;
    }

    /**
     * @return the p1
     */
    public double getP1() {
        return p1;
    }

    /**
     * @param p1 the p1 to set
     */
    public void setP1(double p1) {
        this.p1 = p1;
    }

    /**
     * @return the p2
     */
    public double getP2() {
        return p2;
    }

    /**
     * @param p2 the p2 to set
     */
    public void setP2(double p2) {
        this.p2 = p2;
    }

    /**
     * @return the sumamateria
     */
    public double getSumamateria() {
        return sumamateria;
    }

    /**
     * @param sumamateria the sumamateria to set
     */
    public void setSumamateria(double sumamateria) {
        this.sumamateria = sumamateria;
    }

    /**
     * @return the promediomateria
     */
    public double getPromediomateria() {
        return promediomateria;
    }

    /**
     * @param promediomateria the promediomateria to set
     */
    public void setPromediomateria(double promediomateria) {
        this.promediomateria = promediomateria;
    }

    /**
     * @return the sumamateria2
     */
    public double getSumamateria2() {
        return sumamateria2;
    }

    /**
     * @param sumamateria2 the sumamateria2 to set
     */
    public void setSumamateria2(double sumamateria2) {
        this.sumamateria2 = sumamateria2;
    }

}
