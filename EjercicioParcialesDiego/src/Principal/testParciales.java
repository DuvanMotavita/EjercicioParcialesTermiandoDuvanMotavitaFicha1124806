/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.util.Scanner;
import Clases.Matematicas;
import Clases.Fisica;
import Clases.Quimica;
import Clases.Programacion;
/**
 *
 * @author duvan
 */
public class testParciales {
    public static void main(String [] A)
    {
        //Declaracion de variables para desiciones y que utiliza la clase main
        int desicion;
        int ContadorA=0;
        int ContadorB=0;
        int ContadorC=0;
        int ContadorD=0;
        double enviarpromedio;
        double sumamateria=0;
        double sumamateria2=0;
        double promediomateria;
        double t;
        String Matriz[][] = new String[32][9];
        String Matriz2[][] = new String[32][9];
        String Matriz3[][] = new String[32][9];
        String Matriz4[][] = new String[32][9];
        //Instanciacion de libreria para recibir valores por teclado
        Scanner Leer = new Scanner(System.in);
        //ciclo while para comenzar a realizar operaciones de la clase main
        do
        {
            //informacion del menu
           System.out.println("Por favor elija la materia a la que le desea calcular resultados(Debe tener almenos 1 alumno en todas las materias para que el programa funcione)");
           System.out.println("0.) Salir del sistema ");
           System.out.println("1.) Matematicas");
           System.out.println("2.) Fisica ");
           System.out.println("3.) Quimica ");
           System.out.println("4.) Programacion ");
           //Digitando el numero de desicion
           System.out.println("Digite el numero de la materia que desea calcular:");
           desicion=Leer.nextInt();
           //switch para realizar la accion de alguna desicion
           switch(desicion)
           {
               //caso 0 para salir del while
               case 0:
                   desicion=0;
                   break;
                   //caso 1 de matematicas
               case 1:
                   ContadorA++;
                    //Peticion de datos personales del alumno
                    System.out.println("Digite la indentificacion del alumno");
                    Matriz[ContadorA-1][0]=Leer.next();
                    System.out.println("Digite el nombre del alumno");
                    Matriz[ContadorA-1][1]=Leer.next();
                    System.out.println("Digite el apellido del alumno");
                    Matriz[ContadorA-1][2]=Leer.next();
                    System.out.println("Digite la edad del alumno");
                    Matriz[ContadorA-1][3]=Leer.next();
                    Matematicas m = new Matematicas();
                    m.Calcular();
                    m.setContadorA(ContadorA);
                    //ubicando parcial en la matriz
                    Matriz[ContadorA-1][4]=String.valueOf(m.getP1());
                    //ubicando parcial en la matriz
                    Matriz[ContadorA-1][5]=String.valueOf(m.getP2());
                    //ubicando parcial en la matriz
                    Matriz[ContadorA-1][6]= String.valueOf(m.getP3());
                    //ubicando parcial en la matriz
                    Matriz[ContadorA-1][7]=String.valueOf(m.getP4());
                    //ubicando el promedio total de la materia en la matriz
                    Matriz[ContadorA-1][8]=String.valueOf(m.getPromediototal());
                    //ordena la matriz de abajo hacia arriba
                   for( int i=0; i < ContadorA; i++){
                    for( int j=0;j< 9; j++){
			for(int x=0; x < ContadorA; x++)
                        {
                            for(int y=0; y <9; y++)
                            {
                                if(Double.parseDouble(Matriz[i][8]) > Double.parseDouble(Matriz[x][8]))
                                {
                                    t = Double.parseDouble(Matriz[i][8]);
                                    Matriz[i][8] = Matriz[x][8];
                                    Matriz[x][8] = String.valueOf(t);
                                }
                            }
			}
                     }
                   }
                   System.out.println("*************************************************************************");
                    //enseñando la matriz
                    for(int i=0;i<ContadorA;i++)
                    {
                       for(int j=0;j<9;j++)
                       {
                         System.out.print(Matriz[i][j] + " ");

                       }System.out.println(" ");

                    } System.out.println(" ");
                    System.out.println("*************************************************************************");
                    // calcular promedio
                    sumamateria=sumamateria+Double.parseDouble(Matriz[ContadorA-1][8]);
                    sumamateria2=sumamateria2+sumamateria;
                    promediomateria=sumamateria/ContadorA;
                    System.out.println("El promedio de la materia va en: " + promediomateria);
                   break;
               case 2:
                    ContadorB++;
                    //Peticion de datos personales del alumno
                    System.out.println("Digite la indentificacion del alumno");
                    Matriz2[ContadorB-1][0]=Leer.next();
                    System.out.println("Digite el nombre del alumno");
                    Matriz2[ContadorB-1][1]=Leer.next();
                    System.out.println("Digite el apellido del alumno");
                    Matriz2[ContadorB-1][2]=Leer.next();
                    System.out.println("Digite la edad del alumno");
                    Matriz2[ContadorB-1][3]=Leer.next();
                    Fisica f = new Fisica();
                    f.Calcular();
                    f.setContadorA(ContadorA);
                    //ubicando parcial en la matriz
                    Matriz2[ContadorB-1][4]=String.valueOf(f.getP1());
                    //ubicando parcial en la matriz
                    Matriz2[ContadorB-1][5]=String.valueOf(f.getP2());
                    //ubicando parcial en la matriz
                    Matriz2[ContadorB-1][6]= String.valueOf(f.getP3());
                    //ubicando parcial en la matriz
                    Matriz2[ContadorB-1][7]=String.valueOf(f.getP4());
                    //ubicando el promedio total de la materia en la matriz
                    Matriz2[ContadorB-1][8]=String.valueOf(f.getPromediototal());
                    //ordena la matriz de abajo hacia arriba
                   for( int i=0; i < ContadorB; i++){
                    for( int j=0;j< 9; j++){
			for(int x=0; x < ContadorB; x++)
                        {
                            for(int y=0; y <9; y++)
                            {
                                if(Double.parseDouble(Matriz2[i][8]) > Double.parseDouble(Matriz2[x][8]))
                                {
                                    t = Double.parseDouble(Matriz2[i][8]);
                                    Matriz2[i][8] = Matriz2[x][8];
                                    Matriz2[x][8] = String.valueOf(t);
                                }
                            }
			}
                     }
                   }
                   System.out.println("*************************************************************************");
                    //enseñando la matriz
                    for(int i=0;i<ContadorB;i++)
                    {
                       for(int j=0;j<9;j++)
                       {
                         System.out.print(Matriz2[i][j] + " ");

                       }System.out.println(" ");

                    } System.out.println(" ");
                    System.out.println("*************************************************************************");
                    // calcular promedio
                    sumamateria=sumamateria+Double.parseDouble(Matriz2[ContadorB-1][8]);
                    sumamateria2=sumamateria2+sumamateria;
                    promediomateria=sumamateria/ContadorA;
                    System.out.println("El promedio de la materia va en: " + promediomateria);
                   break;
               case 3:
                    ContadorC++;
                    //Peticion de datos personales del alumno
                    System.out.println("Digite la indentificacion del alumno");
                    Matriz3[ContadorC-1][0]=Leer.next();
                    System.out.println("Digite el nombre del alumno");
                    Matriz3[ContadorC-1][1]=Leer.next();
                    System.out.println("Digite el apellido del alumno");
                    Matriz3[ContadorC-1][2]=Leer.next();
                    System.out.println("Digite la edad del alumno");
                    Matriz3[ContadorC-1][3]=Leer.next();
                    Quimica q = new Quimica();
                    q.Calcular();
                    q.setContadorA(ContadorA);
                    //ubicando parcial en la matriz
                    Matriz3[ContadorC-1][4]=String.valueOf(q.getP1());
                    //ubicando parcial en la matriz
                    Matriz3[ContadorC-1][5]=String.valueOf(q.getP2());
                    //ubicando parcial en la matriz
                    Matriz3[ContadorC-1][6]= String.valueOf(q.getP3());
                    //ubicando parcial en la matriz
                    Matriz3[ContadorC-1][7]=String.valueOf(q.getP4());
                    //ubicando el promedio total de la materia en la matriz
                    Matriz3[ContadorC-1][8]=String.valueOf(q.getPromediototal());
                    //ordena la matriz de abajo hacia arriba
                   for( int i=0; i < ContadorC; i++){
                    for( int j=0;j< 9; j++){
			for(int x=0; x < ContadorC; x++)
                        {
                            for(int y=0; y <9; y++)
                            {
                                if(Double.parseDouble(Matriz3[i][8]) > Double.parseDouble(Matriz3[x][8]))
                                {
                                    t = Double.parseDouble(Matriz3[i][8]);
                                    Matriz3[i][8] = Matriz3[x][8];
                                    Matriz3[x][8] = String.valueOf(t);
                                }
                            }
			}
                     }
                   }
                   System.out.println("*************************************************************************");
                    //enseñando la matriz
                    for(int i=0;i<ContadorC;i++)
                    {
                       for(int j=0;j<9;j++)
                       {
                         System.out.print(Matriz3[i][j] + " ");

                       }System.out.println(" ");

                    } System.out.println(" ");
                    System.out.println("*************************************************************************");
                    // calcular promedio
                    sumamateria=sumamateria+Double.parseDouble(Matriz3[ContadorC-1][8]);
                    sumamateria2=sumamateria2+sumamateria;
                    promediomateria=sumamateria/ContadorA;
                    System.out.println("El promedio de la materia va en: " + promediomateria);
                   break;
               case 4:
                    ContadorD++;
                    //Peticion de datos personales del alumno
                    System.out.println("Digite la indentificacion del alumno");
                    Matriz4[ContadorD-1][0]=Leer.next();
                    System.out.println("Digite el nombre del alumno");
                    Matriz4[ContadorD-1][1]=Leer.next();
                    System.out.println("Digite el apellido del alumno");
                    Matriz4[ContadorD-1][2]=Leer.next();
                    System.out.println("Digite la edad del alumno");
                    Matriz4[ContadorD-1][3]=Leer.next();
                    Programacion p = new Programacion();
                    p.Calcular();
                    p.setContadorA(ContadorA);
                    //ubicando parcial en la matriz
                    Matriz4[ContadorD-1][4]=String.valueOf(p.getP1());
                    //ubicando parcial en la matriz
                    Matriz4[ContadorD-1][5]=String.valueOf(p.getP2());
                    //ubicando parcial en la matriz
                    Matriz4[ContadorD-1][6]= String.valueOf(p.getP3());
                    //ubicando parcial en la matriz
                    Matriz4[ContadorD-1][7]=String.valueOf(p.getP4());
                    //ubicando el promedio total de la materia en la matriz
                    Matriz4[ContadorD-1][8]=String.valueOf(p.getPromediototal());
                    //ordena la matriz de abajo hacia arriba
                   for( int i=0; i < ContadorD; i++){
                    for( int j=0;j< 9; j++){
			for(int x=0; x < ContadorD; x++)
                        {
                            for(int y=0; y <9; y++)
                            {
                                if(Double.parseDouble(Matriz4[i][8]) > Double.parseDouble(Matriz4[x][8]))
                                {
                                    t = Double.parseDouble(Matriz4[i][8]);
                                    Matriz4[i][8] = Matriz4[x][8];
                                    Matriz4[x][8] = String.valueOf(t);
                                }
                            }
			}
                     }
                   }
                   System.out.println("*************************************************************************");
                    //enseñando la matriz
                    for(int i=0;i<ContadorD;i++)
                    {
                       for(int j=0;j<9;j++)
                       {
                         System.out.print(Matriz4[i][j] + " ");

                       }System.out.println(" ");

                    } System.out.println(" ");
                    System.out.println("*************************************************************************");
                    // calcular promedio
                    sumamateria=sumamateria+Double.parseDouble(Matriz4[ContadorD-1][8]);
                    sumamateria2=sumamateria2+sumamateria;
                    promediomateria=sumamateria/ContadorA;
                    System.out.println("El promedio de la materia va en: " + promediomateria);
                   break;
               default : 
                   //mensaje de error si no se encuentra el caso
                   System.out.println("La materia que ingreso no esta dentro de nuestras opciones");
                   break;
           }
           if(desicion==0)
           {        
                    //// Mejor estudiante Matematicas
                    System.out.println("El Mejor estudiante de matematicas es: " + Matriz[0][1]);
                    //// Mejores tres estudiantes Matematicas
                    System.out.println("Los mejores tres en matematicas son : " + Matriz[0][1]+ " , " + Matriz[1][1] + " , " + Matriz[2][1]);
                    //// Peor estudiante Matematicas
                    System.out.println("El peor estudiante de matematicas es: " + Matriz[ContadorA-1][1]);
                    //// peores tres estudiantes estudiante Matematicas
                    System.out.println("Los peores tres estudiantes de matematicas son: " + Matriz[ContadorA-1][1] + " , " + Matriz[ContadorA-2][1] + " , "  + Matriz[ContadorA-3][1] );
                    System.out.println(" ");
                     //// Mejor estudiante Fisica
                    System.out.println("El Mejor estudiante de Fisica es: " + Matriz2[0][1]);
                    //// Mejores tres estudiantes Fisica
                    System.out.println("Los mejores tres en Fisica son : " + Matriz2[0][1]+ " , " + Matriz2[1][1] + " , " + Matriz2[2][1]);
                    //// Peor estudiante Fisica
                    System.out.println("El peor estudiante de Fisica es: " + Matriz2[ContadorB-1][1]);
                    //// peores tres estudiantes estudiante Fisica
                    System.out.println("Los peores tres estudiantes de Fisica son: " + Matriz2[ContadorB-1][1] + " , " + Matriz2[ContadorB-2][1] + " , "  + Matriz2[ContadorB-3][1] );
                    System.out.println(" ");
                     //// Mejor estudiante Quimica
                    System.out.println("El Mejor estudiante de Quimica es: " + Matriz3[0][1]);
                    //// Mejores tres estudiantes Quimica
                    System.out.println("Los mejores tres en Quimica son : " + Matriz3[0][1]+ " , " + Matriz3[1][1] + " , " + Matriz3[2][1]);
                    //// Peor estudiante Quimica
                    System.out.println("El peor estudiante de Quimica es: " + Matriz3[ContadorC-1][1]);
                    //// peores tres estudiantes estudiante Quimica
                    System.out.println("Los peores tres estudiantes de Quimica son: " + Matriz3[ContadorC-1][1] + " , " + Matriz3[ContadorC-2][1] + " , "  + Matriz3[ContadorC-3][1] );
                    System.out.println(" ");
                     //// Mejor estudiante Quimica
                    System.out.println("El Mejor estudiante de Programacion es: " + Matriz4[0][1]);
                    //// Mejores tres estudiantes Quimica
                    System.out.println("Los mejores tres en Programacion son : " + Matriz4[0][1]+ " , " + Matriz4[1][1] + " , " + Matriz4[2][1]);
                    //// Peor estudiante Quimica
                    System.out.println("El peor estudiante de Programacion es: " + Matriz4[ContadorD-1][1]);
                    //// peores tres estudiantes estudiante Quimica
                    System.out.println("Los peores tres estudiantes de Programacion son: " + Matriz4[ContadorD-1][1] + " , " + Matriz4[ContadorD-2][1] + " , "  + Matriz4[ContadorD-3][1] );
                    
                    //Los tres mejores en todas las materias
                    if(Matriz[0][1] == Matriz2[0][1] && Matriz[0][1]==Matriz3[0][1] && Matriz[0][1]==Matriz4[0][1])
                    {
                        System.out.println("Uno de los mejores en todas las areas es:" + Matriz[0][1]);
                    }
                    if(Matriz[1][1] == Matriz2[1][1] && Matriz[1][1]==Matriz3[1][1] && Matriz[1][1]==Matriz4[1][1])
                    {
                        System.out.println("Uno de los mejores en todas las areas es:" + Matriz[1][1]);
                    }
                    if(Matriz[2][1] == Matriz2[2][1] && Matriz[2][1]==Matriz3[2][1] && Matriz[2][1]==Matriz4[2][1])
                    {
                        System.out.println("Uno de los mejores en todas las areas es:" + Matriz[2][1]);
                    }
           }
        }while(desicion!=0);
    }
           
}
